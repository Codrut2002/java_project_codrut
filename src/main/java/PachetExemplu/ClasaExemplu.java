package PachetExemplu;

import java.util.Scanner;

public class ClasaExemplu {
    public static void main(String[] args) {

//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Alege numarul din care sa se scada: ");
//        int a = scanner.nextInt();
//
//        int[] ExArray = {2, 6, 4, 10, 1};
//        for (int i = 0; i < ExArray.length; i++) {
//            if (a > 0) {
//                a -= ExArray[i];
//            }
//        }
//        System.out.println(a);


        //exercitiul 1

//        Scanner scanner1 = new Scanner(System.in);
//        System.out.print("Alege o litera: ");
//        char cuvant = scanner1.next().charAt(0);
//        if (cuvant == 'a' || cuvant == 'e' || cuvant == 'i' || cuvant == 'o' || cuvant == 'u') {
//            System.out.println("Litera este o vocala.");
//        }else {
//            System.out.println("Litera este o consoana.");
//        }

        //exercitiul 2

        Scanner scanner2 = new Scanner(System.in);
        System.out.print("Alege o dimensiune in inch: ");
        int inch = scanner2.nextInt();

        System.out.println("Numarul in cm: " + InchInCm(inch));
    }

    public static double InchInCm(int inch) {
        double cm = 2.54 * inch;

        return cm;
    }
}

