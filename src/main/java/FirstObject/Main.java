package FirstObject;

public class Main {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        System.out.println(calculator.add(2, 3));

        double deimpartit = 50;
        double impartitor = 5;
        double rezultat;
        rezultat = calculator.div(deimpartit, impartitor);
        System.out.println(rezultat);

        int a = 6;
        int b = 3;
        int rezultat2;
        rezultat2 = Operatie.op(a, b);
        System.out.println(rezultat2);

        Operatie operatie = new Operatie();
        operatie.setVar(5);
        System.out.println(operatie.opNotStatic(3));

        System.out.println(Operatie.varStatic);

        Operatie operatie1 = new Operatie();
        Operatie.varStatic = 1;
    }
}
