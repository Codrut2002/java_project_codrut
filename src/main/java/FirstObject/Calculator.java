package FirstObject;

public class Calculator {

    public int add(int a, int b) {
        return a + b;
    }

    public int dif(int a, int b) {
        return a - b;
    }

    public double div(double a, double b) {
        return a / b;
    }

    public int mult(int a, int b) {
        return a * b;
    }
}
