package FirstObject;

public class Operatie {

    private int var;
    public static int varStatic = 3;

    public static int op(int a, int b ) {
        return varStatic * a + varStatic * b;
    }

    public int opNotStatic(int a) {
        return a + var;
    }

    public void setVar(int var) {
        this.var = var;
    }
}
